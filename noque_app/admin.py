from django.contrib import admin

from .models import Store, Category, Product, Customer, Order, OrderItem, StoreProduct

class StoreProductInLine(admin.TabularInline):
    model = StoreProduct
    extra = 3

class OrderItemInline(admin.StackedInline):
    model = OrderItem
    extra = 1

class OrderInLine(admin.TabularInline):
    model = Order
    extra = 1

class StoreAdmin(admin.ModelAdmin):
    fieldsets = [
            (None, {'fields': ['store_name']}),
            ('Address', {'fields': ['location', 'city', 'pincode', 'phone'], 'classes': ['collapse']}),
            ]
    inlines = [StoreProductInLine, OrderInLine]


class CategoryAdmin(admin.ModelAdmin):
    fieldsets = [
            (None, {'fields': ['category_name']}),
            ]

class OrderAdmin(admin.ModelAdmin):
    inlines = (OrderItemInline,)



admin.site.register(Store, StoreAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product)
admin.site.register(Customer)
admin.site.register(Order, OrderAdmin)

