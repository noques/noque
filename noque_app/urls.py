from django.conf.urls import url
# from django.contrib.auth.views import login, logout
from . import views
from rest_framework.authtoken import views as authviews
urlpatterns = [
    url(
        regex=r"^api/$",
        view=views.StoreCreateReadonlyListView.as_view(),
        name="store_rest_api"
    ),
    url(
        regex=r"^api/logout/$",
        view=views.logout_view,
        name="logout_api"
    ),
    url(
        regex=r"^api/customer/signup/$",
        view=views.customer_signup,
        name="customer_signup_api"
    ),
    url(
        regex=r"^api/product/$",
        view=views.ProductReadonlyListView.as_view(),
        name="product_rest_api"
    ),
    url(
        regex=r"^api/user_profile/$",
        view=views.customer_profile,
        name="customer_profile_rest_api"
    ),
    url(
        regex=r"^api/product/(?P<barcode_number>[-\d]+)/$",
        view=views.ProductByBarcodeNumberReadonlyListView.as_view(),
        name="product_by_barcode_number_rest_api"
    ),
    url(
        regex=r"^api/store_products/(?P<store>[-\d]+)/(?P<barcode_number>[-\d]+)/$",
        view=views.StoreProductsWithBarcodeReadonlyView.as_view(),
        name="store_product_rest_api"
    ),
    url(
        regex=r"^api/store_products/(?P<store>[-\d]+)/$",
        view=views.StoreProductsReadonlyView.as_view(),
        name="store_product_rest_api"
    ),
    url(
        regex=r'^api/login/$',
        view=views.login_view,
        name="customer_login_rest_api"
    ),
    url(
        regex=r"^api/(?P<id>[-\d]+)/$",
        view=views.StoreReadonlyView.as_view(),
        name="store_rest_api"
    ),
    url(
        regex=r'^api/store/store_product_list/$',
        view=views.store_product_list,
        name="store_product_list"
    ),
    url(
        regex=r'^api/orders/checkout/$',
        view=views.checkout_order,
        name="checkout_order"
    ),
    url(
        regex=r'^orders/$',
        view=views.get_all_orders,
        name="get_all_order_details_list"
    ),
    url(
        regex=r'^orders/(?P<order_id>[0-9]+)$',
        view=views.get_order,
        name="order_details_list"
    ),
    url(
        regex=r'^orders/place_order/$',
        view=views.place_order,
        name="place_order"
    ),
    url(r'^api-token-auth/', authviews.obtain_auth_token),
]
