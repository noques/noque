from rest_framework import serializers
from .models import Store, StoreProduct, Product, OrderItem


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = ('id', 'store_name', 'location', 'city', 'phone', 'pincode')


class StoreProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = StoreProduct
        fields = ('id', 'store', 'product', 'cost_price', 'store_price', 'quantity_available', 'par_level')
        depth = 1


class StoreProductWithoutStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = StoreProduct
        fields = ('id', 'product', 'cost_price', 'store_price')
        depth = 1


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('product_name', 'short_description', 'maximum_retail_price', 'barcode_number')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'order_date', 'order_status')


