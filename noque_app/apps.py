from django.apps import AppConfig


class NoqueAppConfig(AppConfig):
    name = 'noque_app'
