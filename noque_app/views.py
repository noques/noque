import json
import time
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication, TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from .models import Store, StoreProduct, Product, Customer, Order, OrderItem
from store.models import StoreManager
from .serializers import StoreSerializer, StoreProductSerializer, ProductSerializer, StoreProductWithoutStoreSerializer
from django.core.exceptions import ObjectDoesNotExist
from django.forms.models import model_to_dict

@csrf_exempt
@api_view(['POST'])
def customer_signup(request):
    if request.method == 'POST':
        try:
            str_response = request.body.decode('utf-8')
            data = json.loads(str_response)
            # print(data)
            if not User.objects.filter(username=data.get('username')).exists():
                customer = Customer.create(data)
                user = authenticate(username=data.get('username'), password=data.get('password'))
                login(request, user)
                token = Token.objects.get_or_create(user=customer.user)
                # print(token[0])
                response_data = {'token': str(token[0]),
                                 'message': 'User logged in successfully!!!',
                                 'user_id': customer.user.id,
                                 'username': customer.user.username,
                                 'first_name': customer.user.first_name,
                                 'last_name': customer.user.last_name,
                                 'email': customer.user.email,
                                 'phone': customer.phone_number}
                return Response(data=response_data, status=status.HTTP_201_CREATED)
            else:
                response_data = {'message': 'Username already taken.'}
                return Response(data=response_data, status=status.HTTP_409_CONFLICT)
        except Exception as e:
            print(str(e))
            response_data = {'message': 'Something went wrong. Please try later'}
            return Response(data=response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    else:
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
@api_view(['POST'])
def login_view(request):
    if request.method == 'POST':
        try:
            str_response = request.body.decode('utf-8')
            data = json.loads(str_response)
            username = data.get('username')
            password = data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None and Customer.objects.all().filter(user_id=user.id).exists():
                # print(user.id)
                if user.is_active:
                    login(request, user)
                    customer = Customer.objects.get(user_id=user.id)
                    token = Token.objects.get_or_create(user=user)
                    # print(token[0])
                    response_data = {'token': str(token[0]),
                                     'message': 'User logged in successfully!!!',
                                     'user_id': request.user.id,
                                     'username': request.user.username,
                                     'first_name': request.user.first_name,
                                     'last_name': request.user.last_name,
                                     'email': request.user.email,
                                     'phone': customer.phone_number}
                    return Response(data=response_data, status=status.HTTP_200_OK)
                else:
                    response_data = {'message': 'Sorry, User account is disabled.'}
                    # print(response_data)
                    return Response(data=response_data, status=status.HTTP_401_UNAUTHORIZED)
            else:
                # Return an 'invalid login' error message.
                response_data = {'message': 'Sorry, User is not valid'}
                return Response(data=response_data, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            print(str(e))
            response_data = {'message': 'Something went wrong. Please try later'}
            return Response(data=response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    else:
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_protect
@api_view(['POST'])
def logout_view(request):
    try:
        if request.user.is_authenticated():
            request.user.auth_token.delete()
            logout(request)
            response_data = {'message': 'Logged out successfully!!!'}
            return Response(data=response_data, status=status.HTTP_200_OK)
        else:
            return Response(data={'message': "Session is invalid."},
                            status=status.HTTP_401_UNAUTHORIZED)
    except Exception as e:
        print(str(e))
        return Response(data={'message': 'Something went wrong. Please try again.'},
                        status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET', 'POST'])
def customer_profile(request):
    try:
        if request.user.is_authenticated():
            if Customer.objects.filter(user=request.user).exists():
                customer = Customer.objects.get(user=request.user)
                if request.method == 'GET':
                        response_data = {
                                         'user_id': request.user.id,
                                         'username': request.user.username,
                                         'first_name': request.user.first_name,
                                         'last_name': request.user.last_name,
                                         'email': request.user.email,
                                         'phone': customer.phone_number}
                        return Response(data={'data': response_data,
                                              'message': 'User details fetched successfully!!!'},
                                        status=status.HTTP_200_OK)

                elif request.method == 'POST':
                    str_response = request.body.decode('utf-8')
                    # print(str_response)
                    data = json.loads(str_response)
                    customer.update(data)
                    return Response(data={'message': 'User details updated successfully!!!'},
                                    status=status.HTTP_200_OK)
                else:
                    return Response(status.HTTP_405_METHOD_NOT_ALLOWED)
            else:
                return Response(data={'message': 'User not found.'},
                                status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(data={'message': "Session is invalid."},
                            status=status.HTTP_401_UNAUTHORIZED)

    except ObjectDoesNotExist:
        return Response(data={'message': "User not found"},
                        status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        print(str(e))
        response_data = {'message': 'Something went wrong. Please try later'}
        return Response(data=response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET', 'POST'])
def store_product_list(request):
    """
    Save List of StoreProducts
    :param request:
    :return:
    """

    if request.method == 'POST':
        try:
            if request.user.is_authenticated():
                str_response = request.body.decode('utf-8')
                # print(str_response)
                data = json.loads(str_response)
                store_id = int(data.get('store_id'))
                store_products = data.get('store_products')
                if StoreManager.objects.filter(user=request.user, store=Store.objects.get(pk=store_id)):
                    for store_product in store_products:
                        StoreProduct.create(store_product, int(store_id))
                    return Response(data={'message': "Products added successfully!!!"},
                                    status=status.HTTP_201_CREATED)
                else:
                    return Response(data={'message': 'You do not have permission to perform this action'},
                                    status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response(data={'message': "Session is invalid."},
                                status=status.HTTP_401_UNAUTHORIZED)
        except (Store.DoesNotExist, Product.DoesNotExist, Exception) as e:
            print(str(e))
            return Response(data={'message': "Products could not be added. Try again later"},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    else:
        return Response(data={'message': "Products could not be added. Try again later"},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def checkout_order(request):
    """
    Confirm order checkout after payments
    and update inventory
    :return:
    """
    try:
        if request.user.is_authenticated():
            if request.method == 'POST':
                str_response = request.body.decode('utf-8')
                data = json.loads(str_response)
                order_id = data.get('order_id')
                order = Order.objects.get(pk=order_id)
                order.order_status = 'C'
                order.save()
                for each_order_item in OrderItem.objects.all().filter(order=order):
                    store_product = each_order_item.store_product
                    store_product.quantity_available = store_product.quantity_available - each_order_item.quantity
                    store_product.save()
                return Response(data={'message': "Checkout successful!!!"}, status=status.HTTP_202_ACCEPTED)
            else:
                return Response(data={'message': "Checkout failed. Please try again."},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data={'message': "Session is invalid."},
                            status=status.HTTP_401_UNAUTHORIZED)
    except Exception as e:
        print(str(e))
        return Response(data={'message': "Something went wrong. Please try again."},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def place_order(request):
    """
        To place a order from kart
        :return:order_id
        """
    try:
        if request.user.is_authenticated():
            if request.method == 'POST':
                str_response = request.body.decode('utf-8')
                data = json.loads(str_response)
                print(data)
                """
                    For store products in kart out create a order object
                    and associate order object to all the order items
                """
                order = Order.create(data)
                for each_item in data.get('store_products'):
                    OrderItem.create(each_item, order)
                return Response(data={'message': "Placed order successfully!!!", 'order_id': order.id},
                                status=status.HTTP_202_ACCEPTED)
            else:
                return Response(data={'message': "Placing order failed. Please try again."},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data={'message': "Session is invalid."},
                            status=status.HTTP_401_UNAUTHORIZED)
    except Exception as e:
        print(str(e))
        response_data = {'message': 'Something went wrong. Please try later'}
        return Response(data=response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class StoreCreateReadonlyListView(ListAPIView):
    authentication_classes = (BasicAuthentication, SessionAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class StoreReadonlyView(RetrieveAPIView):
    authentication_classes = (BasicAuthentication, SessionAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
    lookup_field = 'id'


class ProductByBarcodeNumberReadonlyListView(ListAPIView):
    authentication_classes = (BasicAuthentication, SessionAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProductSerializer

    def get_queryset(self):
        barcode_number = self.kwargs['barcode_number']
        return Product.objects.filter(barcode_number__contains=barcode_number)


class ProductReadonlyListView(ListAPIView):
    authentication_classes = (BasicAuthentication, SessionAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class StoreProductsReadonlyView(ListAPIView):
    authentication_classes = (BasicAuthentication, SessionAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = StoreProductSerializer

    def get_queryset(self):
        store = self.kwargs['store']
        return StoreProduct.objects.filter(store=store)


class StoreProductsWithBarcodeReadonlyView(RetrieveAPIView):
    authentication_classes = (BasicAuthentication, SessionAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = StoreProductSerializer
    lookup_field = 'store'

    def get_queryset(self):
        barcode_number = self.kwargs['barcode_number']
        store = self.kwargs['store']
        product = Product.objects.filter(barcode_number=barcode_number)
        return StoreProduct.objects.filter(product=product, store=store)


@api_view(['GET'])
def get_all_orders(request):
    try:
        # print(request.user)
        if request.user.is_authenticated():
            if request.method == 'GET':
                customer = Customer.objects.get(user=request.user)
                order_list = Order.objects.filter(customer=customer).order_by('-id')
                order_response_list = []
                for each_order in order_list:
                    response_dict = dict()
                    response_dict['store'] = model_to_dict(each_order.store)
                    response_dict['order_id'] = each_order.id
                    response_dict['order_date'] = time.mktime(each_order.order_date.timetuple())
                    response_dict['payment_status'] = each_order.order_status
                    order_response_list.append(response_dict)
                return Response(data={'message': 'Orders details fetched successfully!!!',
                                      'data': order_response_list},
                                status=status.HTTP_200_OK)
            else:
                return Response(status.HTTP_405_METHOD_NOT_ALLOWED)
        else:
            return Response(data={'message': "Session is invalid."},
                     status=status.HTTP_401_UNAUTHORIZED)
    except ObjectDoesNotExist:
        return Response(data={'message': "Order not found!!!"},
                        status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        print(str(e))
        response_data = {'message': 'Something went wrong. Please try later'}
        return Response(data=response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def get_response_dict_for_order(order):
    order_item_list = OrderItem.objects.filter(order=order)
    response_data = dict()
    response_data['store_id'] = order.store.id
    response_data['payment_status'] = order.order_status
    order_item_dict_list = []
    for each_item in order_item_list:
        temp_dict = {}
        serializer = StoreProductWithoutStoreSerializer(each_item.store_product)
        temp_dict['store_product'] = serializer.data
        temp_dict['quantity'] = each_item.quantity
        # print(temp_dict)
        order_item_dict_list.append(temp_dict)
    response_data['order_items'] = order_item_dict_list
    return response_data


@api_view(['GET'])
def get_order(request, order_id=None):
    """
        To place a order from kart
        :return:order_id
        """
    try:
        if request.user.is_authenticated():
            if request.method == 'GET':
                order = Order.objects.get(pk=order_id)
                response_data = get_response_dict_for_order(order)
                return Response(data={'message': "Order details fetched successfully!!!", 'data': response_data},
                                status=status.HTTP_200_OK)
            else:
                return Response(data={'message': "Couldn't fetch order details. Please try again."},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data={'message': "Session is invalid."},
                            status=status.HTTP_401_UNAUTHORIZED)
    except ObjectDoesNotExist:
        return Response(data={'message': "Order not found!!!"},
                        status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        print(str(e))
        response_data = {'message': 'Something went wrong. Please try later'}
        return Response(data=response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

