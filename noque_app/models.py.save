from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
import csv, codecs
from datetime import datetime
from django.contrib.auth.models import User


class Store(models.Model):
    store_name = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    phone = models.BigIntegerField(default=0)
    pincode = models.IntegerField(default=0)

    def __str__(self):
        return self.store_name


class Category(models.Model):
    category_name = models.CharField(max_length=30, primary_key=True)

    def __str__(self):
        return self.category_name


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=200)
    short_description = models.CharField(max_length=2000)
    maximum_retail_price = models.FloatField(default=0.0)
    barcode_number = models.    barcode_number = models.BigIntegerField(default=0, unique=True)
(default=0, unique=True)
    image_url = models.CharField(default="", max_length=200, null=True)

    def __str__(self):
        return self.product_name

    def create_and_set_category(self, category_name):
        if Category.objects.all().filter(pk=category_name).exists():
            self.category = Category.objects.get(pk=category_name)
        else:
            category = Category(category_name=category_name)
            self.category = category

    @classmethod
    def create(cls, product_dict):
        category = product_dict['category'] if 'category' in product_dict else "General"
        if not Category.objects.all().filter(pk=category).exists():
            category_obj = Category(category_name=category)
            category_obj.save()
        new_product = cls()
        new_product.category = Category.objects.get(category_name=category)
        new_product.product_name = product_dict['product_name']
        new_product.barcode_number = product_dict['barcode_number']
        new_product.short_description = product_dict['short_description'] \
            if 'short_description' in product_dict else ""
        new_product.maximum_retail_price = product_dict['maximum_retail_price'] \
            if 'maximum_retail_price' in product_dict else 0
        new_product.image_url = product_dict['image_url'] \
            if 'image_url' in product_dict else ""
        new_product.save()

    @classmethod
    def update(cls, product_dict):
        current_product = Product.get(barcode_number=product_dict['barcode_number'])
        current_product.create_and_set_category(product_dict['category'])
        current_product.product_name = product_dict['product_name']
        current_product.short_description = product_dict['short_description']
        current_product.maximum_retail_price = product_dict['maximum_retail_price']
        current_product.barcode_number = product_dict['barcode_number']
        current_product.image_url = product_dict['image_url']
        current_product.save()

    @classmethod
    def addProductsFromCsvFile(cls, file):
        reader = csv.DictReader(codecs.iterdecode(file, "utf-8"))
        products = cls.objects.all()
        for row in reader:
            if products.filter(barcode_number=row['barcode_number']).exists():
                cls.update(row)
            else:
                cls.create(row)


# This code is triggered whenever a new user has been created and saved to the database
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class StoreProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    store = models.ForeignKey(Store, on_delete=models.CASCADE, null=True)
    cost_price = models.IntegerField(default=0)
    store_price = models.IntegerField(default=0)
    quantity_available = models.IntegerField(default=0)
    par_level = models.IntegerField(default=0)

    def _get_imageurl(self):
        "Returns imageurl of the product"
        return self.product.image_url

    def _get_discount(self):
        "Returns the discount calculated by subtracting store_price from MRP"
        return self.product.maximum_retail_price - self.store_price

    @classmethod
    def create(cls, product_dict, store_id):
        if not Product.objects.all().filter(
                    barcode_number=int(product_dict.get('barcode_number'))).exists():
            Product.create(product_dict)

        product = Product.objects.get(barcode_number=int(product_dict.get('barcode_number')))
        store = Store.objects.get(id=int(store_id))
        if StoreProduct.objects.filter(product=product, store=store).exists():
            new_product = StoreProduct.objects.get(store=store, product=product)
        else:
            new_product = cls()
        new_product.cost_price = product_dict['cost_price']
        new_product.store_price = product_dict['store_price']
        new_product.quantity_available = product_dict['quantity_available']
        new_product.product = product
        new_product.store = store
        new_product.save()

    def __str__(self):
        return "{0.store.store_name}: {0.product.product_name}".format(self)


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.BigIntegerField(default=0)

    def __str__(self):
        return self.user.username

    @classmethod
    def create(cls, data):
        user = User.objects.create_user(username=data.get('username'),
                                        email=data.get('email'),
                                        first_name=data.get('first_name'),
                                        last_name=data.get('last_name'),
                                        password=data.get('password'))
        customer = cls()
        customer.user = user
        customer.phone_number = data.get('phone_number')
        return customer


class Order(models.Model):
    ORDER_STATUS = (
        ('P', 'PAYMENT PENDING'),
        ('C', 'COMPLETED'),
        ('R', 'REJECTED')
    )
    order_date = models.DateTimeField("Order time", auto_created=True)
    store = models.ForeignKey(Store)
    customer = models.ForeignKey(Customer)
    store_product = models.ManyToManyField(StoreProduct, through='OrderItem')
    order_status = models.CharField(max_length=30, choices=ORDER_STATUS, default='P')

    def __str__(self):
        return "{}: {} {}".format(self.customer, self.store, self.order_date)

    def is_same_store(self):
        for store_product in self.store_products.all():
            if not self.store == store_product.store:
                return False
        return True

    @classmethod
    def create(cls, data_dict):
        store_id = data_dict.get('store_id')
        new_order = cls()
        new_order.customer = Customer.objects.get(user_id=int(data_dict.get('user_id')))
        new_order.store = Store.objects.get(id=int(store_id))
        new_order.order_date = datetime.now()
        new_order.save()
        return new_order


class OrderItem(models.Model):
    order = models.ForeignKey(Order)
    store_product = models.ForeignKey(StoreProduct)
    quantity = models.IntegerField()

    def __str__(self):
        return "{} {}".format(self.order, self.store_product)

    @classmethod
    def create(cls, data_dict, order):
        new_order = cls()
        new_order.store_product = StoreProduct.objects.get(pk=int(data_dict.get('store_product_id')))
        new_order.order = order
        new_order.quantity = data_dict.get('quantity')
        new_order.save()
        return new_order
