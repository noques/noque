from django import forms
from django.contrib.auth.models import User


class StoreManagerUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name']


class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=30)
    file = forms.FileField()
