from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import StoreManager

class StoreManagerInline(admin.StackedInline):
    model = StoreManager

class UserAdmin(BaseUserAdmin):
    inlines = (StoreManagerInline,)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(StoreManager)
