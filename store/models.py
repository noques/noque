from django.db import models
from django.contrib.auth.models import User
from noque_app.models import Store


class StoreManager(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    store = models.ForeignKey(Store)

    def __str__(self):
        return self.user.username

