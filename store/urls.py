from django.conf.urls import include, url
from rest_framework.authtoken import views as authviews
from . import views

urlpatterns = [
    url(
        regex=r"^login/$",
        view=views.LoginView.as_view(),
        name="login"
    ),
    url(
        regex=r"^register/$",
        view=views.register_store_manager,
        name="register"
    ),
    url(
        regex=r"^welcome/$",
        view=views.WelcomeManagerView.as_view(),
        name="welcome"
    ),
    url(
        regex=r"^update/$",
        view=views.UpdateInformationView.as_view(),
        name="update"
    ),
    url(
        regex=r"^upload-csvfile/$",
        view=views.UploadCsvfileView.as_view(),
        name="upload-csvfile"
    ),
    url(
        regex=r'^api/login/$',
        view=views.login_view_api,
        name="seller_login_rest_api"
    ),
    url('^', include('django.contrib.auth.urls')),
    url(r'^api-token-auth/', authviews.obtain_auth_token),

]
