from django.shortcuts import render_to_response, HttpResponseRedirect, redirect, render
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User, Group
from django.core.urlresolvers import reverse_lazy
from django.template import RequestContext
from django.views.generic import View, TemplateView, FormView
from .forms import StoreManagerUpdateForm, UploadFileForm
from .models import StoreManager
from noque_app.models import Product
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
import json
from rest_framework.response import Response
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt


def index(request):
    return render(request, 'noques.html', {})


class LoginView(View):

    state = "Please login below.."
    username = password = ''

    def post(self, request):
        self.username = request.POST.get('username')
        self.password = request.POST.get('password')

        user = authenticate(username=self.username, password=self.password)
        if user is not None:
            if not user.is_active:
                self.state = "Your account is not active. Please contact site admin..."
            elif not user.has_perm('noque_app.change_store'):
                self.state = "You don't have permissions to access the store. Please contact site admin..."
            else:
                login(request, user)
                self.state = "You are successfully logged in..."
                if len(user.email) == 0 and len(user.first_name) == 0 and \
                        len(user.last_name) == 0:
                    return redirect('update')
                else:
                    return redirect('welcome')
        else:
            self.state = "Your username and/or password were incorrect."
        return render_to_response('store/login.html', {'state': self.state,
            'username': self.username}, context_instance=RequestContext(request))

    def get(self, request):
        return render_to_response('store/login.html', {'state': self.state,
            'username': self.username}, context_instance=RequestContext(request))


class _StoreManagerLoggedInViewMixin(LoginRequiredMixin, PermissionRequiredMixin):

    login_url = "/store/login/"
    permission_required = 'noque_app.change_store'


class UpdateInformationView(_StoreManagerLoggedInViewMixin, FormView):

    template_name = 'store/update-information.html'
    form_class = StoreManagerUpdateForm
    success_url = '/store/welcome'

    def get_initial(self):
        initial = super().get_initial()
        user = self.request.user
        initial['email'] = user.email
        initial['first_name'] = user.first_name
        initial['last_name'] = user.last_name
        return initial

    def get_context_data(self, **kwargs):
        context = super(UpdateInformationView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        data = form.cleaned_data
        user = self.request.user
        user.email = data['email']
        user.first_name = data['first_name']
        user.last_name = data['last_name']
        user.save()
        return super().form_valid(form)


class WelcomeManagerView(_StoreManagerLoggedInViewMixin, TemplateView):

    template_name = 'store/welcome_manager.html'

    def get_context_data(self, **kwargs):
        context = super(WelcomeManagerView, self).get_context_data(**kwargs)
        storeManager = StoreManager.objects.get(user=self.request.user)
        if storeManager != None:
            context['store'] = storeManager.store
        else:
            context['store'] = None
        context['user'] = self.request.user
        return context


class UploadCsvfileView(View):

    form_class = UploadFileForm
    success_url = reverse_lazy('welcome')
    template_name = "store/upload_csvfile.html"

    def get(self,request):
        form = self.form_class
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            file = form.cleaned_data['file']
            Product.addProductsFromCsvFile(file)
            return redirect('welcome')
        else:
            print("Form is not valid")
        return render(request, self.template_name, {'form': form})


# Removing register_user from here. New Store Manager must be created only from Admin panel.
def register_store_manager(request):
    username = password = email = first_name = last_name = ''
    alert = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        confirm_password = request.POST.get('confirm_password')
        email = request.POST.get('email')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')

        if password != None and len(password) >= 6 and password == confirm_password:
            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.groups.add(Group.objects.get(name='StoreManager'))
            user.save()

            state = "Please login below..."
            return render_to_response('store/login.html', {'state': state,
                'username': username}, context_instance=RequestContext(request))
        else:
            alert = "confirm password does not match"

    return render_to_response('store/register.html', {'alert': alert, 'username': username,
        'first_name': first_name, 'last_name': last_name}, context_instance=RequestContext(request))


@csrf_exempt
@api_view(['POST'])
def login_view_api(request):
    if request.method == 'POST':
        try:
            str_response = request.body.decode('utf-8')
            data = json.loads(str_response)
            username = data.get('username')
            password = data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None and StoreManager.objects.all().filter(user_id=user.id).exists():
                if user.is_active:
                    login(request, user)
                    store_user = StoreManager.objects.get(user_id=user.id)
                    token = Token.objects.get_or_create(user=user)
                    # print(request.user.id)
                    response_data = {'token': str(token[0]),
                                     'message': 'User logged in successfully!!!',
                                     'user_id':store_user.user_id,
                                     'store_id':store_user.store_id
                                     }
                    return Response(data=response_data, status=status.HTTP_200_OK)
                else:
                    response_data = {'message': 'Sorry, User account is disabled.'}
                    # print(response_data)
                    return Response(data=response_data, status=status.HTTP_401_UNAUTHORIZED)
            else:
                # Return an 'invalid login' error message.
                response_data = {'message': 'Sorry, User is not valid'}
                return Response(data=response_data, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            print(str(e))
            response_data = {'message': 'Something went wrong. Please try later'}
            return Response(data=response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    else:
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
